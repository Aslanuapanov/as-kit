import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsInput from './AsInput'

export default {
  title: 'Example/Input',
  component: AsInput,
} as ComponentMeta<typeof AsInput>

const Template: ComponentStory<typeof AsInput> = (args) => <AsInput {...args} />

export const Primary = Template.bind({})
export const ErrorInput = Template.bind({})
export const Disabled = Template.bind({})

Primary.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: false,
}

ErrorInput.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: false,
  error: 'the error message',
}

Disabled.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: true,
}
