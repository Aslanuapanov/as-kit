import React, { FC } from 'react'
import './AsInput.scss'

export interface AsInputProps {
  label?: string
  placeholder?: string
  color?: string
  disabled?: boolean
  error?: string
  className?: string
  onChange?: any
  value?: string
}

const AsInput: FC<AsInputProps> = ({
  className,
  color,
  disabled,
  placeholder,
  label,
  error,
  value,
  onChange = () => {},
  ...props
}) => {
  const rootClasses: string[] = ['as-input']

  if (className) {
    rootClasses.push(className)
  }

  if (error) {
    rootClasses.push('as-input--error')
  }

  return (
    <div className="as-wrapper">
      {label ? (
        <label className="as-label" htmlFor="as-input">
          {label}
        </label>
      ) : (
        ''
      )}
      <input
        type="text"
        onChange={onChange}
        value={value}
        className={rootClasses.join(' ')}
        id="as-input"
        placeholder={placeholder}
        {...props}
        style={{ color }}
        disabled={disabled}
      />
      {error ? <p className="error_text">{error}</p> : ''}
    </div>
  )
}

export default AsInput
