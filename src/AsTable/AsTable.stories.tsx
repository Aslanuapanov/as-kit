import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsTable from './AsTable'

export default {
  title: 'Example/Table',
  component: AsTable,
} as ComponentMeta<typeof AsTable>

const Template: ComponentStory<typeof AsTable> = (args) => <AsTable {...args} />

export const Primary = Template.bind({})

Primary.args = {
  table: {
    head: ['ID', 'Сообщения', 'Дата отправки', 'Статус'],
    rows: [
      [
        '586',
        { title: 'Подарок уже ждет вас!', descr: 'Запланированное' },
        new Date().toLocaleDateString(),
        'success',
      ],
      [
        '588',
        {
          title: 'Вы победили в еженедельном розыгрыше, поздравляем!',
          descr: 'По событию',
        },
        new Date().toLocaleDateString(),
        'success',
      ],
      [
        '590',
        {
          title: 'Привет! 👋 Ждем тебя в новом магазине →',
          descr: 'Мгновенное',
        },
        new Date().toLocaleDateString(),
        'waiting',
      ],
      [
        '555',
        { title: 'Новый способ доставки 🚚', descr: 'Регулярное' },
        new Date().toLocaleDateString(),
        'error',
      ],
      [
        '578',
        {
          title: 'Ваша скидка 33% сгорит 3 октября 🔥',
          descr: 'Запланированное',
        },
        new Date().toLocaleDateString(),
        'process',
      ],
    ],
  },
}
