import React, { FC, useState, useCallback } from 'react'
import './AsTable.scss'

interface Table {
  head: string[]
  rows: any[][]
}

export interface AsTableProps {
  table: Table
}

const AsTable: FC<AsTableProps> = ({ table, ...props }) => {
  const rootClasses = ['as-table']

  return (
    <table className={rootClasses.join(' ')}>
      <thead>
        <tr>
          {table.head.map((item, index) => (
            <th key={index}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {table.rows.map((item) => (
          <tr>
            {item.map((content, index) => (
              <td key={index}>
                {index === 1 ? content.title + content.descr : content}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  )
}

export default AsTable
