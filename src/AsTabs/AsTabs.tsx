import React, { FC, useState, useCallback } from 'react'
import './AsTabs.scss'

interface Titles {
  text: string
  content: string
  active: boolean
}

export interface AsTabsProps {
  titles?: Titles[]
}

const AsTabs: FC<AsTabsProps> = ({ titles, ...props }) => {
  const rootClasses = ['as-tabs']
  const [stateTitles, setStateTitles] = useState<Titles[] | any>(titles)

  const updateTitles = useCallback((index: number) => {
    let updateTitles: Titles[] = []
    stateTitles.map((item: Titles, i: number) => {
      let updateTitle = { ...item }

      i === index ? (updateTitle.active = true) : (updateTitle.active = false)
      updateTitles.push(updateTitle)
    })

    setStateTitles(updateTitles)
  }, [])

  return (
    <div className={rootClasses.join(' ')}>
      <div className="as-tabs__title">
        {stateTitles.map((title: Titles, index: number) => (
          <h3
            onClick={() => updateTitles(index)}
            key={index}
            className={title.active ? 'active' : ''}
          >
            {title.text}
          </h3>
        ))}
      </div>
      <div className="as-tabs__descr">
        {stateTitles?.map((title: Titles, index: number) => (
          <p key={index} className={title.active ? 'active' : ''}>
            {title.content}
          </p>
        ))}
      </div>
    </div>
  )
}

export default AsTabs
