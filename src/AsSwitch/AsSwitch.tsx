import React, { FC, useState } from 'react'
import './AsSwitch.scss'

export interface AsSwitchProps {
  color?: string
  children?: string
  primary: boolean
  disabled?: boolean
  size?: string
  checked?: boolean
}

const AsSwitch: FC<AsSwitchProps> = ({
  primary,
  checked,
  size,
  disabled,
  children,
  color,
  ...props
}) => {
  const rootClasses = ['as-switch']
  const [status, setStatus] = useState(checked ? checked : false)

  return (
    <label className={rootClasses.join(' ')}>
      <input
        type="checkbox"
        onChange={() => setStatus(!status)}
        checked={status}
        disabled={disabled}
      />
      <span className="slider round"></span>
    </label>
  )
}

export default AsSwitch
