import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsSwitch from './AsSwitch'

export default {
  title: 'Example/Switch',
  component: AsSwitch,
} as ComponentMeta<typeof AsSwitch>

const Template: ComponentStory<typeof AsSwitch> = (args) => (
  <AsSwitch {...args} />
)

export const Primary = Template.bind({})
export const Disabled = Template.bind({})

Primary.args = {
  primary: true,
  children: 'AsSwitch',
}

Disabled.args = {
  disabled: true,
}
