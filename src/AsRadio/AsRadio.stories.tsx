import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsRadio from './AsRadio'

export default {
  title: 'Example/Radio',
  component: AsRadio,
} as ComponentMeta<typeof AsRadio>

const Template: ComponentStory<typeof AsRadio> = (args) => <AsRadio {...args} />

export const Primary = Template.bind({})
export const Disabled = Template.bind({})

Primary.args = {
  primary: true,
  children: 'AsRadio',
}

Disabled.args = {
  disabled: true,
}
