import React, { FC, useState } from 'react'
import './AsRadio.scss'

export interface AsRadioProps {
  color?: string
  children?: string
  primary: boolean
  disabled?: boolean
  size?: string
  checked?: boolean
}

const AsRadio: FC<AsRadioProps> = ({
  primary,
  checked,
  size,
  disabled,
  children,
  color,
  ...props
}) => {
  const rootClasses = ['as-radio']
  const [status, setStatus] = useState(checked ? checked : false)

  return (
    <label className={rootClasses.join(' ')}>
      <input
        type="checkbox"
        onChange={() => setStatus(!status)}
        checked={status}
        disabled={disabled}
      />
      <span className="checkmark"></span>
    </label>
  )
}

export default AsRadio
