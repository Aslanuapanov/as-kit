import React, { FC } from 'react'
import { Bar } from 'react-chartjs-2'
import { Line } from 'react-chartjs-2'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js'
import './AsCharts.scss'

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  BarElement,
  Title,
  Tooltip,
  Legend,
)

interface ChartData {
  labels: string[]
  datasets: Datasets[]
}

interface Datasets {
  label: string
  data: number[]
  backgroundColor: string
}

export interface AsChartsProps {
  charts: any
  type: string
  data: ChartData
}

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Chart.js Bar Chart',
    },
  },
}

const AsCharts: FC<AsChartsProps> = ({ charts, data, type, ...props }) => {
  const rootClasses = ['as-charts']

  return (
    <div className={rootClasses.join(' ')}>
      {type === 'bar' ? <Bar options={options} data={data} /> : ''}
      {type === 'line' ? <Line options={options} data={data} /> : ''}
    </div>
  )
}

export default AsCharts
