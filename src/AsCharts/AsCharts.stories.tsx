import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsCharts from './AsCharts'

export default {
  title: 'Example/Charts',
  component: AsCharts,
} as ComponentMeta<typeof AsCharts>

const Template: ComponentStory<typeof AsCharts> = (args) => (
  <AsCharts {...args} />
)

export const Bar = Template.bind({})
export const Line = Template.bind({})

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July']

Bar.args = {
  type: 'bar',
  data: {
    labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: labels?.map(() => Math.floor(Math.random() * 100) + 1),
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Dataset 2',
        data: labels?.map(() => Math.floor(Math.random() * 100) + 1),
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ],
  },
}

Line.args = {
  type: 'line',
  data: {
    labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: labels?.map(() => Math.floor(Math.random() * 100) + 1),
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Dataset 2',
        data: labels?.map(() => Math.floor(Math.random() * 100) + 1),
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ],
  },
}
