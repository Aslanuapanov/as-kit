import React, { FC } from 'react'
import './AsTags.scss'

export interface AsTagsProps {
  text?: string
  type?: string
}

const AsTags: FC<AsTagsProps> = ({ text, type, ...props }) => {
  const rootClasses = ['as-tags']

  if (type) {
    rootClasses.push(type)
  }

  return (
    <div className={rootClasses.join(' ')}>
      <div className="circle"></div>
      <div className="as-tags__text">{text}</div>
    </div>
  )
}

export default AsTags
