import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsTags from './AsTags'

export default {
  title: 'Example/Tags',
  component: AsTags,
} as ComponentMeta<typeof AsTags>

const Template: ComponentStory<typeof AsTags> = (args) => <AsTags {...args} />

export const Primary = Template.bind({})
export const Default = Template.bind({})
export const Success = Template.bind({})
export const Warning = Template.bind({})
export const Error = Template.bind({})

Primary.args = {
  text: 'Текст в статуса',
  type: 'primary',
}

Error.args = {
  text: 'Текст в статуса',
  type: 'error',
}

Warning.args = {
  text: 'Текст в статуса',
  type: 'warning',
}

Success.args = {
  text: 'Текст в статуса',
  type: 'success',
}

Default.args = {
  text: 'Текст в статуса',
  type: '',
}
