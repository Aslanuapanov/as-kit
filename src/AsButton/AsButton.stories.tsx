import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsButton from './AsButton'

export default {
  title: 'Example/Button',
  component: AsButton,
} as ComponentMeta<typeof AsButton>

const Template: ComponentStory<typeof AsButton> = (args) => (
  <AsButton {...args} />
)

export const Primary = Template.bind({})
export const Secondary = Template.bind({})
export const Disabled = Template.bind({})
export const Large = Template.bind({})
export const Medium = Template.bind({})
export const Small = Template.bind({})

Primary.args = {
  primary: true,
  children: 'AsButton',
}

Secondary.args = {
  children: 'AsButton',
}

Disabled.args = {
  children: 'AsButton',
  disabled: true,
}

Large.args = {
  primary: true,
  children: 'AsButton',
  disabled: true,
  size: 'large',
}

Medium.args = {
  primary: true,
  children: 'AsButton',
  disabled: true,
  size: 'medium',
}

Small.args = {
  primary: true,
  children: 'AsButton',
  disabled: true,
  size: 'small',
}
