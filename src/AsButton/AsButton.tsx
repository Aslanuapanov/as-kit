import React, { FC } from 'react'
import './AsButton.scss'

export interface AsButtonProps {
  color?: string
  children?: string
  primary?: boolean
  disabled?: boolean
  size?: string
  className?: string
  onClick?: any
}

const AsButton: FC<AsButtonProps> = ({
  className,
  primary,
  size,
  disabled,
  children,
  color,
  onClick = () => {},
  ...props
}) => {
  const rootClasses = ['as-button']

  if (className) {
    rootClasses.push(className)
  }

  if (primary && !disabled) {
    rootClasses.push('as-button--primary')
  } else {
    rootClasses.push('as-button--secondary')
  }

  if (disabled) {
    rootClasses.push('as-button--disabled')
  }

  if (size) {
    rootClasses.push(`as-button--${size}`)
  }

  return (
    <button
      className={rootClasses.join(' ')}
      onClick={onClick}
      {...props}
      style={{ color }}
      disabled={disabled}
    >
      {children}
    </button>
  )
}

export default AsButton
