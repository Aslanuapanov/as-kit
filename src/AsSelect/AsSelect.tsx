import React, { FC, useState } from 'react'
import './AsSelect.scss'

const SelectIcon = require('../assets/img/select_icon.svg')

export interface AsSelectProps {
  items: string[]
  label?: string
  placeholder?: string
  color?: string
  disabled?: boolean
  error?: string
  autocomplete?: string
}

const AsSelect: FC<AsSelectProps> = ({
  items,
  color,
  disabled,
  placeholder,
  autocomplete = 'off',
  label,
  error,
  ...props
}) => {
  const rootClasses = ['as-select']
  const [selectBool, setSelectBool] = useState(false)
  const [selectItem, setSelectItem] = useState('')

  if (error) {
    rootClasses.push('as-select--error')
  }

  const selectItemHandle = (
    event: React.MouseEvent<HTMLElement>,
    value: string,
  ) => {
    setSelectItem(value)
    setSelectBool(false)
  }

  const selects = (event: React.MouseEvent<HTMLElement>) => {
    if (!disabled) {
      setSelectBool(!selectBool)
    }
  }

  return (
    <div className="as-wrapper">
      {label ? (
        <label className="as-label" htmlFor="as-select">
          {label}
        </label>
      ) : (
        ''
      )}
      <div className={rootClasses.join(' ')} onClick={selects}>
        <input
          className="as-select__input"
          type="text"
          id="as-select"
          autoComplete={autocomplete}
          placeholder={placeholder}
          value={selectItem}
          {...props}
          style={{ color }}
          disabled={disabled}
        />
        <img className="as-select__img" src={SelectIcon} alt="" />
      </div>
      {selectBool ? (
        <div className="as-select__items">
          {items ? (
            items.map((item) => (
              <div
                className="as-select__items-list"
                onClick={(e) => selectItemHandle(e, item)}
              >
                {item}
              </div>
            ))
          ) : (
            <div className="as-select__items-no-results">Not data</div>
          )}
        </div>
      ) : (
        ''
      )}
      {error ? <p className="error_text">{error}</p> : ''}
    </div>
  )
}

export default AsSelect
