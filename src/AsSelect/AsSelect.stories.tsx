import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsSelect from './AsSelect'

export default {
  title: 'Example/Select',
  component: AsSelect,
} as ComponentMeta<typeof AsSelect>

const Template: ComponentStory<typeof AsSelect> = (args) => (
  <AsSelect {...args} />
)

export const Primary = Template.bind({})
export const Error = Template.bind({})
export const Disabled = Template.bind({})

Primary.args = {
  label: 'The label',
  items: ['sdfsfsdfdsf', 'sdfsd'],
  placeholder: 'Simple text',
  disabled: false,
}

Error.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: false,
  error: 'the error message',
}

Disabled.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: true,
}
