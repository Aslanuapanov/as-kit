import React, { FC, useState } from 'react'
import './AsCheckbox.scss'

export interface AsCheckboxProps {
  color?: string
  children?: string
  primary: boolean
  disabled?: boolean
  size?: string
  checked?: boolean
}

const AsCheckbox: FC<AsCheckboxProps> = ({
  primary,
  checked,
  size,
  disabled,
  children,
  color,
  ...props
}) => {
  const rootClasses = ['as-checkbox']
  const [status, setStatus] = useState(checked ? checked : false)

  return (
    <label className={rootClasses.join(' ')}>
      <input
        type="checkbox"
        onChange={() => setStatus(!status)}
        checked={status}
        disabled={disabled}
      />
      <span className="checkmark"></span>
    </label>
  )
}

export default AsCheckbox
