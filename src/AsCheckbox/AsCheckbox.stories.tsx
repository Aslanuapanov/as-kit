import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsCheckbox from './AsCheckbox'

export default {
  title: 'Example/Checkbox',
  component: AsCheckbox,
} as ComponentMeta<typeof AsCheckbox>

const Template: ComponentStory<typeof AsCheckbox> = (args) => (
  <AsCheckbox {...args} />
)

export const Primary = Template.bind({})
export const Disabled = Template.bind({})

Primary.args = {
  primary: true,
  children: 'AsCheckbox',
}

Disabled.args = {
  disabled: true,
}
