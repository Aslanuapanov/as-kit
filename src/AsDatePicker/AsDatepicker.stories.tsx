import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsDatepicker from './AsDatepicker'

export default {
  title: 'Example/Datepicker',
  component: AsDatepicker,
} as ComponentMeta<typeof AsDatepicker>

const Template: ComponentStory<typeof AsDatepicker> = (args) => (
  <AsDatepicker {...args} />
)

export const Primary = Template.bind({})

Primary.args = {}
