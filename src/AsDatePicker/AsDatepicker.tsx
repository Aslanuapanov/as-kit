import React, { FC, useState } from 'react'
import DatePicker from 'react-datepicker'

import './AsDatepicker.scss'

export interface AsDatepickerProps {
  label?: string
  disabled?: boolean
  error?: string
}

const AsDatepicker: FC<AsDatepickerProps> = ({
  label,
  error,
  disabled,
  ...props
}) => {
  const rootClasses = ['as-datepicker']
  const [startDate, setStartDate] = useState(new Date())

  return (
    <div className="as-wrapper">
      {label ? (
        <label className="as-label" htmlFor="as-input">
          {label}
        </label>
      ) : (
        ''
      )}
      <DatePicker
        selected={startDate}
        onChange={(date: Date) => setStartDate(date)}
        className={rootClasses.join(' ')}
        id="as-datepicker"
        {...props}
        disabled={disabled}
      />
      {error ? <p className="error_text">{error}</p> : ''}
    </div>
  )
}

export default AsDatepicker
