import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import AsTextarea from './AsTextarea'

export default {
  title: 'Example/Textarea',
  component: AsTextarea,
} as ComponentMeta<typeof AsTextarea>

const Template: ComponentStory<typeof AsTextarea> = (args) => (
  <AsTextarea {...args} />
)

export const Primary = Template.bind({})
export const ErrorInput = Template.bind({})
export const Disabled = Template.bind({})

Primary.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: false,
}

ErrorInput.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: false,
  error: 'the error message',
}

Disabled.args = {
  label: 'The label',
  placeholder: 'Simple text',
  disabled: true,
}
