import React, { FC } from 'react'
import './AsTextarea.scss'

export interface AsTextareaProps {
  label?: string
  placeholder?: string
  color?: string
  disabled?: boolean
  error?: string
}

const AsTextarea: FC<AsTextareaProps> = ({
  color,
  disabled,
  placeholder,
  label,
  error,
  ...props
}) => {
  const rootClasses = ['as-textarea']

  if (error) {
    rootClasses.push('as-textarea--error')
  }

  return (
    <div className="as-wrapper">
      {label ? (
        <label className="as-label" htmlFor="as-textarea">
          {label}
        </label>
      ) : (
        ''
      )}
      <textarea
        className={rootClasses.join(' ')}
        id="as-textarea"
        placeholder={placeholder}
        {...props}
        style={{ color }}
        disabled={disabled}
      ></textarea>
      {error ? <p className="error_text">{error}</p> : ''}
    </div>
  )
}

export default AsTextarea
